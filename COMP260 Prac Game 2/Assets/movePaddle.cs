﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]

public class movePaddle : MonoBehaviour {

    private Rigidbody rBody;

    public float speed = 20f;
    public float force = 10f;

	// Use this for initialization
	void Start () {
        rBody = GetComponent<Rigidbody>();
        rBody.useGravity = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate ()
    {
        //moveMouse();

        moveKeyboard();
    }

    private Vector3 getMousePos ()
    {
        // create a ray from the camera
        //  passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //find out where the ray intersects the XY plane
        Plane plane = new Plane(Vector3.forward, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    private void moveMouse ()
    {
        Vector3 pos = getMousePos();
        Vector3 dir = pos - rBody.position;

        //rBody.AddForce(dir.normalized * force);


        Vector3 vel = dir.normalized * speed;

        //check is this speed going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            vel = vel * distToTarget / move;
        }

        rBody.velocity = vel;

        //dir = dir.normalized;
        //rBody.velocity = dir * speed;

        //rBody.MovePosition(pos);
    }

    private void moveKeyboard ()
    {
        //float move = speed * Time.fixedDeltaTime;

        float x = speed;
        float y = speed;

        x = x * Input.GetAxis("Horizontal");
        y = y * Input.GetAxis("Vertical");

        rBody.velocity = new Vector3(x, y, 0);

    }

    void OnDrawGizmos ()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = getMousePos();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
}
