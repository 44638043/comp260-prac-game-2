﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class audioManager : MonoBehaviour {

	public AudioClip wallCollide;
	public AudioClip paddleCollide;
	public AudioClip win;
	public AudioClip lose;
	
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
		goal[] goals = FindObjectsOfType<goal> ();
		for (int i = 0; i < goals.Length; i++) {
			goals[i].audioEvent += OnAudioEvent;
		}

		puckControl[] pucks = FindObjectsOfType<puckControl> ();
		for (int i = 0; i < pucks.Length; i++) {
			pucks[i].audioEvent += OnAudioEvent;
		}
	}

	public void OnAudioEvent (LayerMask layer, int player) {
		if (player == -1) {
			if (layer == LayerMask.NameToLayer("Paddle")) {
				audio.PlayOneShot(paddleCollide);
			}
			if (layer == LayerMask.NameToLayer("Table")) {
				audio.PlayOneShot(wallCollide);
			}
		} else {
			if (layer == LayerMask.NameToLayer("Goal") && player == 0) {
				audio.PlayOneShot(win);
			}
			if (layer == LayerMask.NameToLayer("Goal") && player == 1) {
				audio.PlayOneShot(lose);
			}
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
