﻿using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(AudioSource))]

public class goal : MonoBehaviour {

    //public AudioClip score;
    //private AudioSource audio;

	// Use this for initialization
	void Start () {
        //audio = GetComponent<AudioSource>();
	}

	public delegate void ScoreGoalHandler(int player);
	public ScoreGoalHandler scoreGoalEvent;
	public int player;
	
	public delegate void audioHandler (LayerMask layer, int player);
	public audioHandler audioEvent;

    void OnTriggerEnter (Collider other)
    {
        //play score sound
        //audio.PlayOneShot(score);

        other.GetComponent<puckControl>().resetPos();

		//notify event handlers if there are any
		if (scoreGoalEvent != null) {
			scoreGoalEvent(player);
		}
		if (audioEvent != null) {
			audioEvent(LayerMask.NameToLayer("Goal"), player);
		}
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
