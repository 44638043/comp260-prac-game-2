﻿using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(AudioSource))]

public class puckControl : MonoBehaviour {

    public Transform startingPos;
    private Rigidbody rBody;

    //public AudioClip wallCollide;
    //public AudioClip paddleCollide;
    public LayerMask paddleLayer;

    //private AudioSource audio;

	// Use this for initialization
	void Start () {
        //audio = GetComponent<AudioSource>();

        rBody = GetComponent<Rigidbody>();
        resetPos();
	}
	
	public delegate void audioHandler (LayerMask layer, int player);
	public audioHandler audioEvent;

    void OnCollisionEnter (Collision col)
	{
		//if (audioEvent != null) {
		if (/*LayerMask.NameToLayer("Paddle").Contains(col.gameObject)*/
		    col.gameObject.layer == LayerMask.NameToLayer("Paddle"))
	        {
	            //audio.PlayOneShot(paddleCollide);
			audioEvent(LayerMask.NameToLayer("Paddle"), -1);
		} else if (col.gameObject.layer == LayerMask.NameToLayer("Table"))
	        {
	            //audio.PlayOneShot(wallCollide);
				audioEvent(LayerMask.NameToLayer("Table"), -1);
	        }
		//}
    }

	// Update is called once per frame
	void Update () {
	
	}

    public void resetPos ()
    {
        rBody.MovePosition(startingPos.position);

        rBody.velocity = Vector3.zero;
    }
}
