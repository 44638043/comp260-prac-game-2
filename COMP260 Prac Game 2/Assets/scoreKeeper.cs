﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class scoreKeeper : MonoBehaviour {

	public int scorePerGoal = 1;
	public int winScore;
	private int[] score = new int[2];
	public Text[] scoreText;
	public Text winText;

	// Use this for initialization
	void Start () {
		// subscribe to events from all the Goals
		goal[] goals = FindObjectsOfType<goal> ();
		for (int i = 0; i < goals.Length; i++) {
			goals[i].scoreGoalEvent += OnScoreGoal;
		}

		//reset the scores to zero
		for (int i = 0; i < score.Length; i ++) {
			score[i] = 0;
			scoreText [i].text = score[i].ToString();
		}

		winText.text = "";
	}

	public void OnScoreGoal (int player) {
		//add points to the plater whose goal it is
		score[player] += scorePerGoal;
		scoreText [player].text = score[player].ToString();
		Debug.Log("Player " + player + ": " + score[player]);

		if (score[player] >= winScore) {
			Time.timeScale = 0;
			winText.text = "PLAYER " + player + " WINS!!!";
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
}
