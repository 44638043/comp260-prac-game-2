﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]

public class paddleAI : MonoBehaviour {

    private Rigidbody rBody;

    public float speed = 20f;
    public float force = 10f;

    public Transform startPos;
    public Transform puck;

    public Vector3 goal;

	// Use this for initialization
	void Start () {
        rBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        float areaAI = 2.5f;

        if (puck.position.x <= areaAI)
        {
            puckCharge();
        }
        else
        {
            goalKeeper();
        }
	}

    void OnCollisionEnter (Collision col)
    {
        if (col.gameObject == puck.gameObject)
        {
            //Debug.Log("HIT");
        }
    }

    private void goalKeeper ()
    {
        Vector3 dir = puck.position - rBody.position;
        Vector3 dir2 = goal - rBody.position;

        Vector3 vel = new Vector3();
        vel.y = dir.normalized.y * speed;
        vel.x = dir2.normalized.x * speed;
        
        //check is this speed going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;
        float distToPost = dir2.magnitude;

        if (move > distToTarget)
        {
            vel.y = vel.y * distToTarget / move;
        }

        if (move > distToPost)
        {
            vel.x = vel.x * distToPost / move;
        }

        rBody.velocity = vel;
        
    }

    private void puckCharge ()
    {
        Vector3 dir = puck.position - rBody.position;

        rBody.AddForce(dir.normalized * force);

        /*Vector3 vel = dir.normalized * speed;

        //check is this speed going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            vel = vel * distToTarget / move;
        }

        rBody.velocity = vel;*/
    }
}
